﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

public class Knight : Chessman
{
    public override bool[,] PossibleMove()
    {
        bool[,] r = new bool[8,8];

        KnightMove(CurrentX - 1, CurrentY + 2, ref r);

        KnightMove(CurrentX + 1, CurrentY + 2, ref r);

        KnightMove(CurrentX +2, CurrentY + 1, ref r);

        KnightMove(CurrentX + 2, CurrentY - 1, ref r);

        KnightMove(CurrentX - 1, CurrentY - 2, ref r);

        KnightMove(CurrentX + 1, CurrentY - 2, ref r);

        KnightMove(CurrentX - 2, CurrentY + 1, ref r);

        KnightMove(CurrentX - 2, CurrentY - 1, ref r);

        return r;
    }

    public void KnightMove(int x, int y, ref bool[,] r)
    {
        Chessman c;
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            c = BoardManager.Instance.Chessmans[x, y];
            if (c == null)
            {
                r[x, y] = true;
            }
            else if (isWhite != c.isWhite)
            {
                r[x, y] = true;
            }
        }
    }
}
