﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Assets.Scripts;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour
{
    public static BoardManager Instance { set; get; }
    private bool [,] allowedMoves { set; get; }

    public Chessman[,] Chessmans { set; get; }
    private Chessman selectedChessman;

    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = 0.5f;

    private int selectionX = -1;
    private int selectionY = -1;

    public List<GameObject> chessmanPrefabs;
    private List<GameObject> activeChessman;

    public int[] EnPassantMove { set; get; }
    
    public bool isWhiteTurn = true;

    public List<Chessman> savedGames = new List<Chessman>();

    public void Start()
    {
        Instance = this;
        Debug.Log(PlayerPrefs.GetString("regime"));
        if (PlayerPrefs.GetString("regime") == "continue")
        {
            //LoadData();
            string[] str = SaveLoad.Load();
            //Debug.Log(str[0]);
            SpawnAllChessmansContinue(str);
            if (PlayerPrefs.GetInt("turn") == 0)
            {
                isWhiteTurn = false;
            }
            else
            {
                isWhiteTurn = true;
            }
        }
        else
        {
            SpawnAllChessmans();
        }
        
        
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
        if (!isWhiteTurn)
        {
            PlayerPrefs.SetInt("turn", 0);
        }else
            PlayerPrefs.SetInt("turn", 1);
    }

    private void Update()
    {
        UpdateSelection();
        DrawChessboard();
        if (Input.GetMouseButtonDown(0))
        {
            if (selectionX >= 0 && selectionY >= 0)
            {
                if (selectedChessman == null)
                {
                    SelectChessman(selectionX, selectionY);
                }
                else
                {
                    MoveChessman(selectionX, selectionY);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackToMenu();
        }

    }

    private void SelectChessman(int x, int y)
    {
        if (Chessmans[x, y] == null)
        {
            return;
        }
        if (Chessmans[x, y].isWhite != isWhiteTurn)
        {
            return;
        }

        bool hasAtleastOneMove = false;
        allowedMoves = Chessmans[x, y].PossibleMove();
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (allowedMoves[i, j])
                {
                    hasAtleastOneMove = true;
                }
            }
        }
        if (!hasAtleastOneMove)
        {
            return;
        }
        selectedChessman = Chessmans[x, y];
        Debug.Log(selectedChessman.CurrentX + " " + selectedChessman.CurrentY);
        BoardHighlights.Instance.HighlightAllowedMoves(allowedMoves);
    }

    private void MoveChessman(int x, int y)
    {
        if (allowedMoves[x,y])
        {
            Chessman c = Chessmans[x, y];
            if (c != null && c.isWhite != isWhiteTurn)
            {

                if (c.GetType() == typeof(King))
                {
                    EndGame();
                    return;
                }

                activeChessman.Remove(c.gameObject);
                Debug.Log(c);
                SaveLoad.DeleteFromTxt(c);
                Destroy(c.gameObject);
            }

            if (x == EnPassantMove[0] && y == EnPassantMove[1])
            {
                if (isWhiteTurn)
                {
                    c = Chessmans[x, y-1];
                    activeChessman.Remove(c.gameObject);
                    Debug.Log(c);
                    //SaveLoad.SaveXml(c);
                    Destroy(c.gameObject);
                }
                else
                {
                    c = Chessmans[x, y + 1];
                    activeChessman.Remove(c.gameObject);
                    Debug.Log(c);
                    //SaveLoad.SaveXml(c);
                    Destroy(c.gameObject);
                }
            }
            EnPassantMove[0] = -1;
            EnPassantMove[1] = -1;
            if (selectedChessman.GetType() == typeof(Pawn))
            {
                if (y == 7)
                {
                    activeChessman.Remove(selectedChessman.gameObject);
                    Destroy(selectedChessman.gameObject);
                    SpawnChessman(1,x,y);
                    selectedChessman = Chessmans[x, y];
                }
                else if (y == 0)
                {
                    activeChessman.Remove(selectedChessman.gameObject);
                    Destroy(selectedChessman.gameObject);
                    SpawnChessman(7, x, y);
                    selectedChessman = Chessmans[x, y];
                }

                if (selectedChessman.CurrentY == 1 && y == 3)
                {
                    EnPassantMove[0] = x;
                    EnPassantMove[1] = y - 1;
                }
                else if (selectedChessman.CurrentY == 6 && y == 4)
                {
                    EnPassantMove[0] = x;
                    EnPassantMove[1] = y + 1;
                }
            }
            SaveLoad.DeleteFromTxt(selectedChessman);
            Chessmans [selectedChessman.CurrentX, selectedChessman.CurrentY] = null;
            selectedChessman.transform.position = GetTileCenter(x, y);
            selectedChessman.SetPosition(x, y);
            Chessmans [x, y] = selectedChessman;
            isWhiteTurn = !isWhiteTurn;
            string datapath = Application.persistentDataPath + "/savedGames.xml";
            SaveLoad.UpdateTxt(selectedChessman, Chessmans[x, y]);
           // Debug.Log(Chessmans[x, y]);
        }
        BoardHighlights.Instance.Hidehighlights();
        selectedChessman = null;
    }
    
    private void UpdateSelection()
    {
        if (!Camera.main)
        {
            return;
        }

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f,
            LayerMask.GetMask("ChessPlane")))
        {
            selectionX = (int) hit.point.x;
            selectionY = (int) hit.point.z;
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        }

    }

    private void SpawnChessman(int index, int x, int y)
    {
        GameObject go = Instantiate(chessmanPrefabs[index], GetTileCenter(x,y), Quaternion.identity) as GameObject;
        go.transform.SetParent(transform);
        Chessmans[x, y] = go.GetComponent<Chessman>();
        Chessmans[x, y].SetPosition(x,y);
        activeChessman.Add(go);
        SaveGame(Chessmans[x, y]);
        SaveLoad.SaveTxt(Chessmans[x, y]);
    }

    private void SpawnAllChessmansContinue(string[] lines)
    {
        SaveLoad.DeleteAll();
        activeChessman = new List<GameObject>();
        Chessmans = new Chessman[8,8];
        EnPassantMove = new int[2] {-1,-1};
        //Debug.Log(lines.Length);
        //Spawn the white team!
        int i = 0, j = 0;
        int[][] result = new int[lines.Length][];
        foreach (var row in lines)
        {
            j = 0;
            if (i < lines.Length)
            {
                result[i] = new int[3];
                foreach (var col in row.Trim().Split('\t'))
                {
                    if (row != "")
                    {
                        //Console.Write(col);
                        result[i][j] = Convert.ToInt32(col);
                        Debug.Log(result[i][1]);
                        j++;
                    }
                }
                SpawnChessman(result[i][0], result[i][1], result[i][2]);
                i++;
            }
        }
    }


    private void SpawnAllChessmans()
    {
        SaveLoad.DeleteAll();
        activeChessman = new List<GameObject>();
        Chessmans = new Chessman[8, 8];
        EnPassantMove = new int[2] { -1, -1 };
        //Debug.Log(lines.Length);
         SpawnChessman(0,3,0);

         SpawnChessman(1,4,0);

         SpawnChessman(2,0,0);
         SpawnChessman(2,7,0);

         SpawnChessman(3,2,0);
         SpawnChessman(3,5,0);

         SpawnChessman(4,1,0);
         SpawnChessman(4,6,0);

         for (int ii = 0; ii < 8; ii++)
         {
             SpawnChessman(5,ii,1);
         }

         SpawnChessman(6,4,7);

         SpawnChessman(7,3,7);

         SpawnChessman(8,0,7);
         SpawnChessman(8,7,7);

         SpawnChessman(9,2,7);
         SpawnChessman(9,5,7);

         SpawnChessman(10,1,7);
         SpawnChessman(10,6,7);

         for (int ii = 0; ii < 8; ii++)
         {
             SpawnChessman(11,ii,6);
         }
    }


    private Vector3 GetTileCenter(int x, int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (TILE_SIZE*x) + TILE_OFFSET;
        origin.z += (TILE_SIZE*y) + TILE_OFFSET;
        return origin;
    }

    private void DrawChessboard()
    {
        Vector3 widhtLine = Vector3.right * 8;
        Vector3 heightLine = Vector3.forward * 8;

        for (int i = 0; i <=8; i++)
        {
            Vector3 start = Vector3.forward*i;
            Debug.DrawLine(start, start + widhtLine);
            for (int j = 0; j <=8; j++)
            {
                start = Vector3.right * j;
                Debug.DrawLine(start, start + heightLine);
            }
        }

        if (selectionX >= 0 && selectionY >= 0)
        {
            Debug.DrawLine(Vector3.forward * selectionY + Vector3.right * selectionX,
                Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));

            Debug.DrawLine(Vector3.forward * (selectionY + 1) + Vector3.right * selectionX,
                Vector3.forward * selectionY + Vector3.right * (selectionX + 1));

        }
    }

    private void EndGame()
    {
        Debug.Log(isWhiteTurn ? "White team wins" : "Black team wins");

        foreach (GameObject go in activeChessman)
        {
            Destroy(go);
        }

        isWhiteTurn = true;
        BoardHighlights.Instance.Hidehighlights();
        SpawnAllChessmans();
    }

    public void SaveGame(Chessman chessman)
    {
        savedGames.Add(chessman);
    }
   
}
