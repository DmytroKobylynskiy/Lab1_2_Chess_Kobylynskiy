﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts
{
    static class SaveLoad
    {
        static Dictionary<string,int> Chassmans = new Dictionary <string,int> () {
            {"White King(Clone) (King)",0 },
            {"White Queen(Clone) (Queen)",1 },
            {"White Rook(Clone) (Rook)",2 },
            {"White Bishop(Clone) (Bishop)",3 },
            {"White Knight(Clone) (Knight)",4 },
            {"White Pawn(Clone) (Pawn)",5 },
            {"Black King(Clone) (King)",6 },
            {"Black Queen(Clone) (Queen)",7 },
            {"Black Rook(Clone) (Rook)",8 },
            {"Black Bishop(Clone) (Bishop)",9 },
            {"Black Knight(Clone) (Knight)",10 },
            {"Black Pawn(Clone) (Pawn)",11 },
        };

        static public void DeleteAll()
        {
                File.WriteAllText("Saves/save.txt", String.Empty);
        }

        static public void SaveTxt(Chessman data)
        {
            //Debug.Log(data);
            var id = Chassmans[data.ToString()];
            string chessmanToSave = id + "\t" + data.CurrentX + "\t" + data.CurrentY;
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter("Saves/save.txt", true))
            {
                file.WriteLine(chessmanToSave);
            }
        }

        static public void UpdateTxt(Chessman previous,Chessman data)
        {
            Debug.Log(previous.CurrentX + " " + previous.CurrentY);
            var id = Chassmans[previous.ToString()];
            string chessmanToDelete = id + "\t" + previous.CurrentX + "\t" + previous.CurrentY;
            string chessmanToUpdate = Chassmans[data.ToString()] + "\t" + data.CurrentX + "\t" + data.CurrentY;
            var lines = File.ReadAllLines("Saves/save.txt").Where(line => line.Trim() != chessmanToDelete).ToList();
            lines.Add(chessmanToUpdate);
            File.WriteAllLines("Saves/save.txt",lines.ToArray());
        }

        static public void DeleteFromTxt(Chessman data)
        {
            //Debug.Log(data);
            var id = Chassmans[data.ToString()];
            string chessmanToDelete = id + "\t" + data.CurrentX + "\t" + data.CurrentY;
            var lines = File.ReadAllLines("Saves/save.txt").Where(line => line.Trim() != chessmanToDelete).ToArray();
            File.WriteAllLines("Saves/save.txt",lines);
            
        }

        static public string[] Load()
        {

            string[] lines = System.IO.File.ReadAllLines("Saves/save.txt");

            return lines;
        }
    }
}
